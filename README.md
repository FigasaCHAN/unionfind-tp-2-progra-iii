# UnionFind - TP 2 Progra III 

El objetivo del trabajo práctico es medir la diferencia en los tiempos de ejecución del Algoritmo
de Kruskal para el problema de árbol generador mínimo con y sin el uso de la estructura de
datos Union-Find. Para esto, se piden los siguientes puntos:
1. Implementar el Algoritmo de Kruskal sin Union-Find (es decir, determinando con BFS
si dos vértices están en la misma componente conexa).
2. Implementar el Algoritmo de Kruskal con alguna de las versiones de Union-Find, implementada sobre un arreglo de índices.
3. Generar grafos aleatoriamente y medir el tiempo de ejecución de las dos versiones.
4. Graficar el tiempo promedio de ejecución en función del tamaño del grafo.
Se recomienda realizar la generación aleatoria de grafos por medio del siguiente modelo. Dada
una cantidad de n vértices y una cantidad m de aristas, comenzar con un grafo de n vértices
sin aristas y agregar cada arista entre dos vértices seleccionados aleatoriamente. El peso de
la arista se puede generar como un valor real con distribución uniforme entre 0 y 1.
Para tomar los tiempos de ejecución, se recomienda ejecutar el algoritmo varias veces sobre
la misma instancia, y dividir el tiempo total por la cantidad de ejecuciones. Se recomienda
también realizar varias mediciones con grafos distintos del mismo tamaño.

Como objetivos adicionales no obligatorios, se pueden considerar los siguientes puntos:
1. Implementar distintas versiones de Union-Find y evaluar su impacto sobre la performance del algoritmo.
2. Considerar más de un mecanismo para generar grafos aleatoriamente, y evaluar su
impacto sobre la performance del algoritmo.
3. Implementar también el Algoritmo de Prim y comparar los tiempos.
