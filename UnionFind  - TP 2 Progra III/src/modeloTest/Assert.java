package modeloTest;

import java.util.Set;
import static org.junit.Assert.*;

public class Assert {
	public static <K> void conjuntoEsElEsperado(K[] esperado, Set<K> conjunto)
	{
		//dada un array, se fija si es igual al conjunto
		boolean todosPertenecen= true;
		for ( K elem: esperado) {
			todosPertenecen&= conjunto.contains(elem);
		}
		assertTrue(todosPertenecen&& (esperado.length == conjunto.size()));
	}

}
