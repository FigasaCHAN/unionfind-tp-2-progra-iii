package modeloTest;

import static org.junit.Assert.*;

import org.junit.Test;

import modelo.UnionFindV2;

public class UnionFindV2Test {

	@Test
	public void raizTest() {
		UnionFindV2 uf2 = inicializarEjemplo(); 
		int[] raices = {0,1,2,3,4};
		System.out.println(uf2.raiz(4));
		assertEquals(raices[4], uf2.raiz(4));
	}
	private UnionFindV2 inicializarEjemplo() {
		UnionFindV2 uf2 = new UnionFindV2(5);
		return uf2;
	}
}
