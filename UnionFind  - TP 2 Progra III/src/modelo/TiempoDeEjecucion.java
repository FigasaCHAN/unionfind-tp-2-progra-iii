package modelo;

public class TiempoDeEjecucion {
	public static long kruskalBFS(GrafoConPeso grafo) {
		long TInicio, TFin, tiempo;
		TInicio= System.nanoTime(); 
		ArbolGeneradorMinimo.kruskalConBFS(grafo);
		TFin= System.nanoTime(); 
		tiempo= TFin - TInicio;
		return tiempo;
	}
	public static long kruskalConUnionFindV1(GrafoConPeso grafo) {
		long TInicio, TFin, tiempo;
		TInicio= System.nanoTime(); 
		ArbolGeneradorMinimo.unionFindV1(grafo);
		TFin= System.nanoTime(); 
		tiempo= TFin - TInicio;
		return tiempo;
	}
	public static long kruskalConUnionFindV2(GrafoConPeso grafo) {
		long TInicio, TFin, tiempo;
		TInicio= System.nanoTime(); 
		ArbolGeneradorMinimo.unionFindV2(grafo);
		TFin= System.nanoTime(); 
		tiempo= TFin - TInicio;
		return tiempo;
	}
	public static long promedioKruskalBFS(GrafoConPeso grafo, int cantDeVeces) {
		long tiempo= 0;
		for(int i=0; i<cantDeVeces; i++) {
			tiempo+= kruskalBFS(grafo);
		}
		return tiempo/cantDeVeces;
	}
	public static long promedioKruskalConUnionFindV1(GrafoConPeso grafo,int cantDeVeces) {
		long tiempo= 0;
		for(int i=0; i<cantDeVeces; i++) {
			tiempo+= kruskalConUnionFindV1(grafo);
		}
		return tiempo/cantDeVeces;
	}
	public static long promedioKruskalConUnionFindV2(GrafoConPeso grafo,int cantDeVeces) {
		long tiempo= 0;
		for(int i=0; i<cantDeVeces; i++) {
			tiempo+= kruskalConUnionFindV2(grafo);
		}
		return tiempo/cantDeVeces;
	}
	public static long kruskalBFSOptimizado(GrafoConPeso grafo) {
		long TInicio, TFin, tiempo;
		TInicio= System.nanoTime(); 
		ArbolGeneradorMinimo.kruskalConBFSOptimizado(grafo);
		TFin= System.nanoTime(); 
		tiempo= TFin - TInicio;
		return tiempo;
	}
	public static long kruskalConUnionFindV1Optimizado(GrafoConPeso grafo) {
		long TInicio, TFin, tiempo;
		TInicio= System.nanoTime(); 
		ArbolGeneradorMinimo.unionFindV1Optimizado(grafo);
		TFin= System.nanoTime(); 
		tiempo= TFin - TInicio;
		return tiempo;
	}
	public static long kruskalConUnionFindV2Optimizado(GrafoConPeso grafo) {
		long TInicio, TFin, tiempo;
		TInicio= System.nanoTime(); 
		ArbolGeneradorMinimo.unionFindV2Optimizado(grafo);
		TFin= System.nanoTime(); 
		tiempo= TFin - TInicio;
		return tiempo;
	}
	public static long promedioKruskalBFSOptimizado(GrafoConPeso grafo, int cantDeVeces) {
		long tiempo= 0;
		for(int i=0; i<cantDeVeces; i++) {
			tiempo+= kruskalBFSOptimizado(grafo);
		}
		return tiempo/cantDeVeces;
	}
	public static long promedioKruskalConUnionFindV1Optimizado(GrafoConPeso grafo,int cantDeVeces) {
		long tiempo= 0;
		for(int i=0; i<cantDeVeces; i++) {
			tiempo+= kruskalConUnionFindV1Optimizado(grafo);
		}
		return tiempo/cantDeVeces;
	}
	public static long promedioKruskalConUnionFindV2Optimizado(GrafoConPeso grafo,int cantDeVeces) {
		long tiempo= 0;
		for(int i=0; i<cantDeVeces; i++) {
			tiempo+= kruskalConUnionFindV2Optimizado(grafo);
		}
		return tiempo/cantDeVeces;
	}
	
}
