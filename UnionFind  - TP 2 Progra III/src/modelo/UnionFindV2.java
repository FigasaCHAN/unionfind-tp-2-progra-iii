package modelo;

public class UnionFindV2 extends UnionFind{

	public UnionFindV2(int cantidadDeVertices) {
		super(cantidadDeVertices);
	}
	@Override
	public int raiz(int i) {
		while(this.raices[i]!=i) {
			int aux= i;
			i= this.raices[i];
			this.raices[aux]=  this.raices[i]; //el anterior ahora apunta al abuelo, es decir al padre del actual
		}
		return i;
	}
}
