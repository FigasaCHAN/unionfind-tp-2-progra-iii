
package modelo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class ArbolGeneradorMinimo { 
	public static GrafoConPeso kruskalConBFS(GrafoConPeso grafo){
		validarGrafoConexo(grafo);
		GrafoConPeso arbolGeneradorMinimo= new GrafoConPeso(grafo.cantidadDeVertices); //va a tener la misma cant de vertices
		Set<Arista>aristasDisponibles= new HashSet<Arista>();
		aristasDisponibles.addAll(grafo.getAristas());
		int i= 0;
		while(i<grafo.getCantidadDeVertices()-1) {//por teorema de Arbol
			Arista masChica= aristaMasChica(grafo,aristasDisponibles); //busca la arista mas chica del grafo
			if(!perteneceALaMismaComponenteConexa(arbolGeneradorMinimo,masChica)) { //se fija si no hace circuito
				arbolGeneradorMinimo.agregarArista(masChica.getVerticeI(),masChica.getVerticeJ(), grafo.peso(masChica.getVerticeI(), masChica.getVerticeJ()));
				i++;//paso al siguiente
			}
			aristasDisponibles.remove(masChica); //la arista ya no esta disponible
		}
		return arbolGeneradorMinimo;
	}
	public static GrafoConPeso kruskalConBFSOptimizado(GrafoConPeso grafo){
		validarGrafoConexo(grafo);
		GrafoConPeso arbolGeneradorMinimo= new GrafoConPeso(grafo.cantidadDeVertices); //va a tener la misma cant de vertice
		ArrayList<Arista> listaOrdenada= new ArrayList<Arista>();
		listaOrdenada.addAll(grafo.getAristas());
		Collections.sort(listaOrdenada);
		Iterator<Arista> it= listaOrdenada.iterator();
		Arista masChica= it.hasNext()? it.next() : null;
		int i= 0;
		while(i<grafo.getCantidadDeVertices()-1) {//por teorema de Arbol
			if(!perteneceALaMismaComponenteConexa(arbolGeneradorMinimo,masChica)) { //se fija si no hace circuito
				arbolGeneradorMinimo.agregarArista(masChica.getVerticeI(),masChica.getVerticeJ(), grafo.peso(masChica.getVerticeI(), masChica.getVerticeJ()));
				i++;//paso al siguiente
			}
			masChica= it.hasNext()? it.next() : null;
		}
		return arbolGeneradorMinimo;
	}
	public static GrafoConPeso unionFindV1(GrafoConPeso grafo) {
		return unionFindAlgoritmo(grafo,1);
	}
	public static GrafoConPeso unionFindV2(GrafoConPeso grafo) {
		return unionFindAlgoritmo(grafo,2);
	}
	public static GrafoConPeso unionFindV1Optimizado(GrafoConPeso grafo) {
		return unionFindAlgoritmoOptimizado(grafo,1);
	}
	public static GrafoConPeso unionFindV2Optimizado(GrafoConPeso grafo) {
		return unionFindAlgoritmoOptimizado(grafo,2);
	}
	private static GrafoConPeso unionFindAlgoritmo(GrafoConPeso grafo, int version) {
		if(version<1 && version>2) {
			throw new IllegalArgumentException("Solo hay dos versiones de UnionFind");
		}
		validarGrafoConexo(grafo);
		UnionFind arbolGeneradorMinimo= null;
		if(version==1) {
			arbolGeneradorMinimo= new UnionFind(grafo.getCantidadDeVertices());
		}
		if(version==2) {
			arbolGeneradorMinimo= new UnionFindV2(grafo.getCantidadDeVertices());
		}

		Set<Arista>aristasDisponibles= new HashSet<Arista>();
		aristasDisponibles.addAll(grafo.getAristas());

		int i= 0;
		while(i<grafo.getCantidadDeVertices()-1) {//por teorema de Arbol
			Arista masChica= aristaMasChica(grafo,aristasDisponibles); //busca la arista mas chica del grafo
			int pesoDeMasChica= grafo.peso(masChica.getVerticeI(), masChica.getVerticeJ());
			if(!arbolGeneradorMinimo.find(masChica.getVerticeI() , masChica.getVerticeJ())) { //si NO estan en la misma componente conexa
				arbolGeneradorMinimo.agregarArista(masChica.getVerticeI(), masChica.getVerticeJ(), pesoDeMasChica);
				i++;
			}
			aristasDisponibles.remove(masChica); //la arista ya no esta disponible
		}
		return arbolGeneradorMinimo.getGrafo();//devuelvo el grafo
	}
	private static GrafoConPeso unionFindAlgoritmoOptimizado(GrafoConPeso grafo, int version) {
		if(version<1 && version>2) {
			throw new IllegalArgumentException("Solo hay dos versiones de UnionFind");
		}
		validarGrafoConexo(grafo);
		UnionFind arbolGeneradorMinimo= null;
		if(version==1) {
			arbolGeneradorMinimo= new UnionFind(grafo.getCantidadDeVertices());
		}
		if(version==2) {
			arbolGeneradorMinimo= new UnionFindV2(grafo.getCantidadDeVertices());
		}

		ArrayList<Arista> listaOrdenada= new ArrayList<Arista>();
		listaOrdenada.addAll(grafo.getAristas());
		Collections.sort(listaOrdenada);
		Iterator<Arista> it= listaOrdenada.iterator();
		Arista masChica= it.hasNext()? it.next() : null;
		int i= 0;
		while(i<grafo.getCantidadDeVertices()-1) {//por teorema de Arbol
			int pesoDeMasChica= grafo.peso(masChica.getVerticeI(), masChica.getVerticeJ());
			if(!arbolGeneradorMinimo.find(masChica.getVerticeI() , masChica.getVerticeJ())) { //si NO estan en la misma componente conexa
				arbolGeneradorMinimo.agregarArista(masChica.getVerticeI(), masChica.getVerticeJ(), pesoDeMasChica);
				i++;
			}
			masChica= it.hasNext()? it.next() : null;
		}
		return arbolGeneradorMinimo.getGrafo();//devuelvo el grafo
	}

	public static Arista aristaMasChica(GrafoConPeso grafo, Set <Arista> aristas) {
		//recibe el grafo y el conjunto de aristas que se desea buscar cual es la arista mas chica
		//busca cual es la arista mas chica dentro del grafo
		if(grafo.getAristas().size()==0) {
			throw new IllegalArgumentException("El grafo no tiene aristas, por lo tanto no se puede buscar la arista mas chica");
		}
		Arista masChica= aristas.iterator().next(); //va a agarrar el primer elem
		for(Arista elem : aristas) {//todas las aristas
			int pesoMasChica= grafo.peso(masChica.getVerticeI(), masChica.getVerticeJ());
			int pesoAComparar= grafo.peso(elem.getVerticeI(), elem.getVerticeJ());
			if(pesoAComparar!=-1 && pesoMasChica>pesoAComparar) { //si pesoAComparar es -1 significa que la arista no existe en el grafo o no tiene definido el peso
				masChica= elem;
			}
		}
		return masChica;
	}
	private static void validarGrafoConexo(GrafoConPeso grafo) {
		if(!grafo.esConexo()) {
			throw new IllegalArgumentException("El grafo debe ser conexo para hacer un ArbolGeneradorMinimo");
		}
	}
	private static boolean perteneceALaMismaComponenteConexa(GrafoConPeso arbol, Arista arista) {
		//dado un grafo y una arista, se fija si desde el verticeI existe un camino hasta el verticeJ
		//la arista puede estar en el grafo como no
		for(int elem : BFS.alcanzables( arbol, arista.getVerticeI()) ) {//si desde el verticeI
			if(elem==arista.getVerticeJ()) {//puedo llegar al verticeJ
				return true;
			}
		}
		return false;
	}
}
