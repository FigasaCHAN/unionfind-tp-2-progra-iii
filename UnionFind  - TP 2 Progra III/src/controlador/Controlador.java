package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JSpinner;

import modelo.ArbolGeneradorMinimo;
import modelo.GrafoConPeso;
import modelo.TiempoDeEjecucion;
import vista.VentanaPrincipal;

public class Controlador {
	private VentanaPrincipal ventana;
	private JButton btnGenerar;
	public Controlador(VentanaPrincipal ventana) {
		this.ventana= ventana;
		this.btnGenerar= this.ventana.getHubPanel().getBtnGenerar();
		this.btnGenerar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int cantDeVertices= ventana.cantidadDeVertices();
				int cantDeAristas= ventana.cantidadDeAristas();
				
				GrafoConPeso grafo= new GrafoConPeso(cantDeVertices,cantDeAristas, true );
				
				long tiempoBFS= TiempoDeEjecucion.promedioKruskalBFS(grafo,30);
				long tiempoUFV1= TiempoDeEjecucion.promedioKruskalConUnionFindV1(grafo,30);
				long tiempoUFV2= TiempoDeEjecucion.promedioKruskalConUnionFindV2(grafo,30);
				long tiempoBFSOptimizado= TiempoDeEjecucion.promedioKruskalBFSOptimizado(grafo,30);
				long tiempoUFV1Optimizado= TiempoDeEjecucion.promedioKruskalConUnionFindV1Optimizado(grafo,30);
				long tiempoUFV2Optimizado= TiempoDeEjecucion.promedioKruskalConUnionFindV2Optimizado(grafo,30);
				/*System.out.println(ArbolGeneradorMinimo.kruskalConBFS(grafo).getAristas());
				System.out.println(ArbolGeneradorMinimo.kruskalConBFSOptimizado(grafo).getAristas());
				System.out.println(ArbolGeneradorMinimo.unionFindV1(grafo).getAristas());
				System.out.println(ArbolGeneradorMinimo.unionFindV1Optimizado(grafo).getAristas());
				System.out.println(ArbolGeneradorMinimo.unionFindV2(grafo).getAristas());
				System.out.println(ArbolGeneradorMinimo.unionFindV2Optimizado(grafo).getAristas());*/
				ventana.actualizarGrafica(tiempoBFS,tiempoUFV1,tiempoUFV2,tiempoBFSOptimizado,tiempoUFV1Optimizado,tiempoUFV2Optimizado);
			}
		});
	}
	public void mostrarVentanaPrincipal() {
		this.ventana.mostrar();
	}
	
}
