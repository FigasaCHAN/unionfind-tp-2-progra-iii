package main;

import controlador.Controlador;
import vista.VentanaPrincipal;

public class Main {

	public static void main(String[] args){
		VentanaPrincipal ventana= new VentanaPrincipal();
		Controlador controlador= new Controlador(ventana);
		controlador.mostrarVentanaPrincipal();
	}

}
